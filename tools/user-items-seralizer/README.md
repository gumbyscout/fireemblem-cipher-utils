# user-items-seralizer

## Usage
- Install Node.js 6
- Install app dependencies
```
$ npm install
```
- Copy fireemblem-cipher-decks and fireemblem-cipher-collection folders or .db files to this directory (usually found somewhere in AppData or the home folder)
- Run the app
```
$ node user-itmes-seralizer.js
```
- You should now have two folders in this directory, collections and decks
- Import appropriate files into the app
