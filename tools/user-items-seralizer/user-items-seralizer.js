'use strict';

const jsonfile = require('jsonfile');
const mkdirp = require('mkdirp');
const PouchDB = require('pouchdb-node');
PouchDB.plugin(require('pouchdb-adapter-node-websql'));

const isEmpty = require('lodash/fp/isEmpty');
const snakeCase = require('lodash/fp/snakeCase');
const map = require('lodash/fp/map');
const pick = require('lodash/fp/pick');

const collectionDBName = 'fireemblem-cipher-collection';
const decksDBName = 'fireemblem-cipher-decks';

const collectionDBs = [
  new PouchDB(collectionDBName),
  new PouchDB(collectionDBName + '.db', {adapter: 'websql'})
];
const decksDBs = [
  new PouchDB(decksDBName),
  new PouchDB(decksDBName + '.db', {adapter: 'websql'})
];

const serializeCollections = () => {
  collectionDBs.map((db, index) => {
    db.allDocs({"include_docs": true}).then(resp => {
      let collection = map('doc', resp.rows);
      if (!isEmpty(collection)) {
        mkdirp.sync('collections');
        jsonfile.writeFileSync(
          `collections/${collectionDBName}-${index}.fecucl`,
          {collection: map(pick(['count', '_id']))(collection)}
        );
      }
    });
  });
};

const serializeDecks = () => {
  decksDBs.map(db => {
    db.allDocs({"include_docs": true}).then(resp => {
      let decks = map('doc', resp.rows);
      if (!isEmpty(decks)) {
        mkdirp.sync('decks');
        decks.map(deck => {
          jsonfile.writeFileSync(
            `decks/${snakeCase(deck.name)}.fecudk`,
            pick(['name', 'cards'])(deck)
          );
        });
      }
    });
  });
};

serializeCollections();
serializeDecks();
