const Xray = require('x-ray');
const htmlToText = require('html-to-text');
const Fuse = require('fuse.js');
const sharp = require('sharp');
const RateLimiter = require('limiter').RateLimiter;
const request = require('request-promise');

const flatten = require('lodash/flatten');
const camelCase = require('lodash/camelCase');
const assign = require('lodash/assign');
const reduce = require('lodash/reduce');
const trim = require('lodash/trim');
const filter = require('lodash/filter');
const map = require('lodash/map');
const isEmpty = require('lodash/isEmpty');

const groupBy = require('lodash/fp/groupBy');

const Card = require('../../src/models/Card');

/* eslint-disable no-console, no-magic-numbers */
class SerenesForrestWiki {
  static scrapePage (page) {
    const x = Xray({
      filters: {
        trim: (string) => trim(string, ' \n\t'), // eslint-disable-line lodash-fp/no-extraneous-args
        noPromote: (string) => string.replace(/\((promoted|unpromoted|non-promotable|unromoted)\)/gi, '')
      }
    });
    return x(page, {
      cards: x('.sf', [{
        name: 'tr:first-child | noPromote | trim',
        imageUrl: 'tr:nth-child(2) img@src',
        class: 'tr:nth-child(3) td:nth-child(2) | trim',
        range: 'tr:nth-child(3) td:nth-child(4) | trim',
        insignia: 'tr:nth-child(4) td:nth-child(2) | trim',
        gender: 'tr:nth-child(4) td:nth-child(4) | trim',
        weapon: 'tr:nth-child(5) td:nth-child(2) | trim',
        other: 'tr:nth-child(5) td:nth-child(4) | trim',
        cost: 'tr:nth-child(6) td:nth-child(2) | trim',
        promotion: 'tr:nth-child(6) td:nth-child(4) | trim',
        attack: 'tr:nth-child(7) td:nth-child(2) | trim',
        support: 'tr:nth-child(7) td:nth-child(4) | trim',
        series: 'tr:nth-child(8) td:nth-child(2) | trim',
        illustrator: 'tr:nth-child(8) td:nth-child(4) | trim',
        miscRows: x('tr:nth-child(n+9)', [{
          label: 'td:nth-child(1)',
          value: 'td:nth-child(2)@html | trim'
        }])
      }])
    });
  }

  static scrapeBoosterPage (url) {
    const x = Xray();
    return new Promise((resolve, reject) =>
      x(url, {
        pages: x('.MsoTableGrid td:nth-child(3)', [{
          url: 'a@href',
          name: 'a'
        }])
      })((error, pages) => {
        if (error) {
          reject(error);
        }
        resolve(pages);
      })
    )
    .catch(error => console.error('pages', error))
    .then(data => data.pages)
    .then(groupBy('url'));
  }

  static scrapeStarterPage (url) {
    const x = Xray();
    return new Promise((resolve, reject) =>
      x(url, {
        tables: x('.MsoTableGrid', [{
          pages: x('td:nth-child(4)', [{
            url: 'a@href',
            name: 'a'
          }])
        }])
      })((error, data) => {
        if (error) {
          reject(error);
        }
        resolve(data);
      })
    )
    .catch(error => console.error('pages', error))
    .then(data => data.tables[0].pages)
    .then(groupBy('url'));
  }

  static scrapePages (pages) {
    return Promise.all(map(pages, (cards, url) =>
      new Promise((resolve, reject) =>
        SerenesForrestWiki.scrapePage(url)((error, object) => {
          if (error) {
            reject(error);
          } else {
            console.log('scraped page', url);
            let fuse = new Fuse(cards, {
              keys: ['name'],
              threshold: 0.05
            });
            let outCards = filter(object.cards, card => {
              try {
                return !isEmpty(fuse.search(card.name));
              } catch (e) {
                console.error(e, card);
                return false;
              }
            });
            resolve(outCards);
          }
        })
      )
    ))
    .catch(error => console.error('cards', error))
    .then(flatten);
  }

  static downloadImage (card) {
    /* eslint-disable no-console, no-magic-numbers */
    console.log('fetching image');
    return new Promise((resolve, reject) => {
      SerenesForrestWiki.limiter.removeTokens(1, () => {
        SerenesForrestWiki.imgRequest.get(card.imageUrl)
          .catch((error) => {
            console.log('failed to encode');
            reject(error);
          })
          .then(image => {
            console.log('downloaded image');
            let imgBuffer = new Buffer.from(image);
            sharp(imgBuffer)
              .resize(310, 435)
              .webp().toBuffer()
              .then(buff => buff.toString('base64'))
              .then(image => {
                card._attachments = {
                  thumbnail: {
                    'content_type': 'image/webp',
                    'data': image
                  }
                };
                console.log('encoded image');
                resolve();
              });
          });
      });
    });
    /* eslint-enable */
  }

  static update (pages) {
    return SerenesForrestWiki.scrapePages(pages)
    .then(cards => cards.map(card => {
      // clean up some of the data
      let miscRows = card.miscRows.map(({label, value}) => {
        return {
          [camelCase(label)]: htmlToText
            .fromString(value, {ignoreHref: true})
            .replace(/\[(?:https?|ftp):\/\/[\n\S]+/g, '')
        };
      });
      miscRows = reduce(miscRows, (object, row) => assign(object, row), {});
      console.log('converted misc rows');
      return assign(card, miscRows);
    }))
    .then(cards => cards.map(card => Card.fromSerenesWikiCard(card)))
    .then(cards => {
      return Promise.all(cards.map(card => SerenesForrestWiki.downloadImage(card)))
      .then(() => {
        return cards;
      });
    });
  }
}

SerenesForrestWiki.limiter = new RateLimiter(1, 100);
SerenesForrestWiki.imgRequest = request.defaults({encoding: null});

module.exports = SerenesForrestWiki;
