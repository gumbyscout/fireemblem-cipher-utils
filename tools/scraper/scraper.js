#!/usr/bin/node
'use strict';

const fs = require('fs-extra');
const jsonfile = require('jsonfile');
const yargs = require('yargs');
const sanitize = require('sanitize-filename');

const map = require('lodash/fp/map');
const flow = require('lodash/fp/flow');
const omit = require('lodash/fp/omit');
const assign = require('lodash/fp/assign');
const concat = require('lodash/fp/concat');
const uniqBy = require('lodash/fp/uniqBy');
const orderBy = require('lodash/fp/orderBy');
const isEmpty = require('lodash/fp/isEmpty');

const SerenesForrestWiki = require('./serenesForrestWiki');

const scrapedFileName = 'scraped.json';
const dbFileName = 'db.json';

/* eslint-disable no-console */
var outJSON = (cards) => {
  return new Promise((resolve, reject) => {
    const baseDir = 'images';
    fs.ensureDirSync(baseDir);
    fs.emptyDirSync(baseDir);

    let cleanedCards = map(card => {
      if (isEmpty(card._id)) {
        console.error(card);
        return;
      }
      let file = sanitize(card._id + '_thumb').replace(/\W/g, '');
      let fileName = `${baseDir}/${file}.webp`;
      try {
        fs.writeFileSync(fileName, card._attachments.thumbnail.data, {encoding: 'base64'});
      } catch (e) {
        console.error('writeFileSync', e);
      }
      return flow(
        omit('_attachments'),
        assign({
          thumbnail: file
        })
      )(card);
    })(cards);

    jsonfile.writeFile(scrapedFileName, {cards: cleanedCards}, {spaces: 2}, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

let commonOptions = {
  test: {
    alias: 't',
    describe: 'Test run, don\'t generate db JSON',
    boolean: true
  }
};

let commonCardHandler = argv => cards => {
  let promises = [];
  if (!argv.test) {
    promises.push(outJSON(cards));
  }
  return Promise.all(promises)
  .then(() => console.log(`Scrapped ${cards.length} cards`));
};

yargs
  .command({
    command: 'booster <url>',
    desc: 'Scrape a booster page on the Serenes Forrest Wiki',
    builder: commonOptions,
    handler: argv => {
      SerenesForrestWiki.scrapeBoosterPage(argv.url)
      .then(SerenesForrestWiki.update)
      .then(commonCardHandler(argv))
      .catch(console.error);
    }
  });

yargs
  .command({
    command: 'card <url> <name>',
    desc: 'Scrape a specific card on the Serenes Forrest Wiki',
    builder: commonOptions,
    handler: argv => {
      SerenesForrestWiki.update({[argv.url]: [{name: argv.name, url: argv.url}]})
      .then(commonCardHandler(argv))
      .catch(console.error);
    }
  });

yargs
  .command({
    command: 'starter <url>',
    desc: 'Scrape a starter deck for exclusive cards on the Serenes Forrest Wiki',
    builder: commonOptions,
    handler: argv => {
      SerenesForrestWiki.scrapeStarterPage(argv.url)
      .then(SerenesForrestWiki.update)
      .then(commonCardHandler(argv))
      .catch(console.error);
    }
  });


yargs
  .command({
    command: 'add-scraped',
    desc: 'Add the docs from the scraped db file to the db file',
    handler: () => {
      let scraped = require(`./${scrapedFileName}`).cards,
          db = require(`./${dbFileName}`).cards;

      let cards = flow(
        concat(scraped),
        uniqBy('_id'),
        map(omit(['_rev', 'scrapedData'])),
        orderBy('_id')(['asc'])
      )(db);

      jsonfile.writeFileSync(`${dbFileName}.bak`, {cards: db}, {spaces: 2}, (err) => {
        if (err) {
          console.err(err);
        }
      });

      jsonfile.writeFile(dbFileName, {cards}, {spaces: 2}, (err) => {
        if (err) {
          console.err(err);
        }
      });
    }
  });

yargs
  .help()
  .argv;
