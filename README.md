# Fire Emblem Cipher Utils

A web app with various utilities to manage Fire Emblem 0 (Cipher) decks and cards.

Fire Emblem 0 (Cipher) is a trading card game created by Intelligent Systems in conjuction with Nintendo based on the Fire Emblem series created by the same companies. It is only released in Japan, and is only available in Japanese.

![Marth](resources/Card.jpg "Lodestar, Marth")

Fire Emblem Cipher Utils serves to be a all-in-one application for all English speaking fans to be able to organize and play the trading card game.

- Search our database for all the details you need to know about a card. Featuring valuable Japanese-to-English card translations!
- Create, save, and share decks with the available cards. Keep track of all the decks you have, or plan out one you want to have!
- Create cheat sheets! Create a printout of your decklist so you can reference it whenever you play the game. The cheatsheet features all of the key information you need to properly play the card.
- Keep track of your current card collection with the Collection Manager.

<img src="resources/CardSearch.jpg" alt="Searching for Cards" style="width: 800px;" />

<img src="resources/CardDetail.jpg" alt="Card Detail Page" style="width: 800px;" />

<img src="resources/CheatSheetSnagit.jpg" alt="The cheatsheet, all its glory!" style="width: 800px;" />

---

### Contact

If you find issues, please report them in the GitHub's Issues area of this project or on the post at the Serenes Forest Forums [here](http://serenesforest.net/forums/index.php?showtopic=64481).

---

### Credits

- [SerenesForest.net](https://serenesforest.net) for being THE go to resource of Fire Emblem information
