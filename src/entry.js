require("babel-polyfill");

import("./components/Router").then(({ default: Router }) => {
  require("react-virtualized/styles.css");
  require("react-selectize/themes/index.css");
  require("react-tap-event-plugin")();

  const React = require("react");
  const ReactDOM = require("react-dom");
  ReactDOM.render(
    React.createElement(Router),
    document.getElementById("react-root")
  );
});
