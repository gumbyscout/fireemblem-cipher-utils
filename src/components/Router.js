import React from "react";
import { Router, Route, IndexRoute, hashHistory } from "react-router";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import thunk from "redux-thunk";
import promise from "redux-promise";
import logger from "redux-logger";

import rootReducer from "../reducers";
import rootSaga from "../rootSaga";

import App from "./App";
import CardSearch from "./CardSearch";
import { CardDetail } from "../CardDetail";
import { ManageDecks } from "../ManageDecks";
import { Collection } from "../Collection";
import FAQ from "./FAQ";
import Print from "./Print";

const sagaMiddleWare = createSagaMiddleware();

let middleWare = [thunk, promise, sagaMiddleWare];

if (process.env.NODE_ENV === "development") {
  middleWare = [logger, ...middleWare];
}

const store = createStore(rootReducer, applyMiddleware(...middleWare));

sagaMiddleWare.run(rootSaga);

var RouterComp = () => (
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/print" component={Print} />
      <Route path="/" component={App}>
        <IndexRoute component={CardSearch} />
        <Route path="card/:id" component={CardDetail} />
        <Route path="decks" component={ManageDecks} />
        <Route path="collection" component={Collection} />
        <Route path="faq" component={FAQ} />
      </Route>
    </Router>
  </Provider>
);

export default RouterComp;
