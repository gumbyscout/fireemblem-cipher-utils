import React from "react";
import PropTypes from "prop-types";

const ExternalLink = ({ link, children }) => (
  <a target="_blank" href={link}>
    {children}
  </a>
);

ExternalLink.propTypes = {
  link: PropTypes.string,
  children: PropTypes.any
};

export default ExternalLink;
