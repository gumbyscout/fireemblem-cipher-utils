import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router";

import { map, isEmpty, flow } from "lodash/fp";
const mapUncapped = map.convert({ cap: false });

import {
  cardsInCurrentDeckSelector,
  countOfCardsInCurrentDeckSelector,
  currentDeckColorCountsSelector
} from "../reducers/App";
import {
  currentDeckSelector,
  updateCurrentDeck,
  deleteDeck
} from "../ManageDecks";

import { List, ListItem } from "material-ui/List";

import InlineCountBadge from "./InlineCountBadge";
import DrawerItem from "./DrawerItem";
import CardIconUtils from "../services/cardIconUtils";

import IconButton from "material-ui/IconButton";
import DeleteIcon from "material-ui/svg-icons/action/delete";

import styles from "./DeckDrawer.pcss";

const DeckDrawer = ({
  currentDeck,
  cardsInCurrentDeck = [],
  total,
  colorCounts,
  renameDeck,
  deleteDeck,
  router
}) => {
  return (
    <div className={styles.drawer}>
      <div className={styles["deck-meta"]}>
        {!isEmpty(currentDeck) ? (
          <div>
            <div className={styles["deck-label"]}>
              <input
                className={styles["deck-name"]}
                value={currentDeck.name}
                onChange={e => renameDeck(e.target.value)}
              />

              <IconButton onClick={() => deleteDeck(currentDeck)}>
                <DeleteIcon />
              </IconButton>
            </div>
            <div className={styles["deck-info"]}>
              <InlineCountBadge count={total} /> |
              {mapUncapped((count, set) => (
                <span key={set}>
                  <img
                    src={CardIconUtils.getSetIcon(set)}
                    className={styles["set-icon"]}
                  />
                  <InlineCountBadge count={count} />
                </span>
              ))(colorCounts)}
            </div>
          </div>
        ) : (
          <ListItem onClick={() => router.push("decks")}>
            <span className={styles["white-text"]}>Select a deck</span>
          </ListItem>
        )}
      </div>
      <div className={styles["deck-items"]}>
        <List>
          {cardsInCurrentDeck.map(
            ({ _id, name, title, set, mainCharacter, count }) => (
              <DrawerItem
                key={_id}
                id={_id}
                name={name}
                title={title}
                set={set}
                mainCharacter={mainCharacter}
                count={count}
              />
            )
          )}
        </List>
      </div>
    </div>
  );
};

DeckDrawer.propTypes = {
  renameDeck: PropTypes.func.isRequired,
  currentDeck: PropTypes.object.isRequired,
  cardsInCurrentDeck: PropTypes.array.isRequired,
  total: PropTypes.number.isRequired,
  colorCounts: PropTypes.object.isRequired,
  deleteDeck: PropTypes.func.isRequired,
  router: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  currentDeck: currentDeckSelector(state),
  cardsInCurrentDeck: cardsInCurrentDeckSelector(state),
  total: countOfCardsInCurrentDeckSelector(state),
  colorCounts: currentDeckColorCountsSelector(state)
});

const mapDispatchToProps = dispatch => ({
  renameDeck: name => dispatch(updateCurrentDeck(deck => ({ ...deck, name }))),
  deleteDeck: deck => dispatch(deleteDeck(deck))
});

export default flow(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter
)(DeckDrawer);
