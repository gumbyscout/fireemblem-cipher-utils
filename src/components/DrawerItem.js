import React from "react";
import PropTypes from "prop-types";
import CSSModules from "react-css-modules";

import { ListItem } from "material-ui/List";
import Avatar from "material-ui/Avatar";

import CardIconUtils from "../services/cardIconUtils";
import fireEmblem from "../resources/fire-emblem.svg";
import InlineCountBadge from "./InlineCountBadge";

import styles from "./DrawerItem.pcss";

const emblemStyles = {
  stroke: "black",
  fill: "black",
  zIndex: "-1",
  width: "24px",
  height: "24px",
  position: "absolute",
  left: "0",
  top: "0"
};

@CSSModules(styles)
class DrawerItem extends React.Component {
  render () {
    const { set, name, title, id, count, mainCharacter } = this.props;
    return (
      <ListItem
        leftAvatar={
          <Avatar
            style={{
              backgroundColor: "initial",
              borderRadius: "initial"
            }}
            src={CardIconUtils.getSetIcon(set)}
          />
        }
        primaryText={name}
        secondaryText={title}
        rightIcon={
          <span>
            {mainCharacter ? (
              <svg style={emblemStyles}>
                <use xlinkHref={`${fireEmblem}#svg2`} />
              </svg>
            ) : null}
            <InlineCountBadge count={count} noBackground={mainCharacter} />
          </span>
        }
        onClick={() => {
          this.context.router.push(`card/${encodeURIComponent(id)}`);
        }}
      />
    );
  }
}

DrawerItem.contextTypes = {
  router: PropTypes.object.isRequired
};

DrawerItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  set: PropTypes.string.isRequired,
  title: PropTypes.string,
  mainCharacter: PropTypes.bool,
  count: PropTypes.number,
  styles: PropTypes.object
};

export default DrawerItem;
