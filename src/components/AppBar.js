import React from "react";
import PropTypes from "prop-types";
import { IndexLink, Link } from "react-router";
import { connect } from "react-redux";

import { toggleDrawer } from "../actions/App";

import AppBarMaterial from "material-ui/AppBar";
import FlatButton from "material-ui/FlatButton";
import IconButton from "material-ui/IconButton";
import NavigationMenu from "material-ui/svg-icons/navigation/menu";

import logo from "../resources/logo.svg";

const VERSION = require("../../package.json").version;

const styles = {
  appBar: {
    WebkitAppRegion: "drag",
    paddingLeft: "1rem"
  },
  appBarButtons: {
    WebkitAppRegion: "no-drag",
    display: "flex",
    alignItems: "center"
  },
  iconRightStyle: {
    marginTop: "initial",
    display: "flex"
  },
  flatButton: {
    color: "#fff"
  },
  logo: {
    height: "50px",
    width: "50px",
    fill: "#fff",
    verticalAlign: "middle"
  }
};

const AppBar = ({ toggleDrawer }) => (
  <AppBarMaterial
    iconElementLeft={
      <IconButton onClick={toggleDrawer}>
        <NavigationMenu />
      </IconButton>
    }
    iconStyleRight={styles.iconRightStyle}
    style={styles.appBar}
    title={
      <span>
        <svg style={styles.logo}>
          <use xlinkHref={`${logo}#logo`} />
        </svg>
        Fire Emblem Cipher Utils v{VERSION}
      </span>
    }
    iconElementRight={
      <span style={styles.appBarButtons}>
        <Link to="/print" target="_blank">
          <FlatButton labelStyle={styles.flatButton} label="Print Deck" />
        </Link>
        <IndexLink to="/">
          <FlatButton labelStyle={styles.flatButton} label="Search" />
        </IndexLink>
        <Link to="/decks">
          <FlatButton labelStyle={styles.flatButton} label="Decks" />
        </Link>
      </span>
    }
  />
);

AppBar.propTypes = {
  toggleDrawer: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  toggleDrawer: () => dispatch(toggleDrawer())
});

export default connect(
  null,
  mapDispatchToProps
)(AppBar);
