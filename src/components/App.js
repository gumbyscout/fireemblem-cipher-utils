import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import getMuiTheme from "material-ui/styles/getMuiTheme";

import { getCards } from "../actions/App";
import AppBar from "./AppBar";
import DeckDrawer from "./DeckDrawer";
import Sidebar from "react-sidebar";

const styles = {
  app: {
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    left: 0,
    top: 0
  },
  noLinkStyle: {
    textDecoration: "none",
    color: "inherit"
  },
  sideBar: {
    root: {
      top: "64px"
    },
    content: {
      overflow: "hidden",
      display: "flex",
      flexDirection: "column"
    },
    sidebar: {
      width: "353px",
      display: "flex",
      overflow: "hidden",
      flexDirection: "column",
      flex: 1
    }
  }
};

class App extends React.Component {
  componentDidMount () {
    this.props.getCards();
  }

  getChildContext () {
    return {
      muiTheme: getMuiTheme({
        palette: require("../theme")
      })
    };
  }

  render () {
    const { children, drawerIsOpen } = this.props;
    return (
      <div style={styles.app}>
        <AppBar />
        <Sidebar
          styles={styles.sideBar}
          open={drawerIsOpen}
          docked={drawerIsOpen}
          sidebar={<DeckDrawer />}
        >
          {children}
        </Sidebar>
      </div>
    );
  }
}

App.childContextTypes = {
  muiTheme: PropTypes.object.isRequired
};

App.propTypes = {
  getCards: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  drawerIsOpen: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  drawerIsOpen: state.App.drawerIsOpen
});

const mapDispatchToProps = dispatch => ({
  getCards: doUpdate => dispatch(getCards(doUpdate))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
