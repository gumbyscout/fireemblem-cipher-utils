import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Fuse from "fuse.js";
import CSSModules from "react-css-modules";

import isMatch from "lodash/isMatch";
import debounce from "lodash/debounce";

import isEmpty from "lodash/fp/isEmpty";
import filter from "lodash/fp/filter";
import orderBy from "lodash/fp/orderBy";
import map from "lodash/fp/map";
import flow from "lodash/fp/flow";
import includes from "lodash/fp/includes";
import intersection from "lodash/fp/intersection";

import { Grid, AutoSizer } from "react-virtualized";
import Paper from "material-ui/Paper";
import CircularProgress from "material-ui/CircularProgress";
import Search from "material-ui/svg-icons/action/search";

import { setRow, setFilter, setQuery, setSort } from "../actions/CardSearch";
import CardThumbListItem from "./CardThumbListItem";
import CardSearchFilterBar from "./CardSearchFilterBar";

import styles from "./CardSearch.pcss";

@CSSModules(styles)
class CardSearch extends React.Component {
  static propTypes = {
    cards: PropTypes.array.isRequired,
    filter: PropTypes.array.isRequired,
    query: PropTypes.string.isRequired,
    setFilter: PropTypes.func.isRequired,
    setQuery: PropTypes.func.isRequired,
    row: PropTypes.number.isRequired,
    setRow: PropTypes.func.isRequired,
    sort: PropTypes.object.isRequired,
    setSort: PropTypes.func.isRequired
  };

  static contextTypes = {
    router: PropTypes.object.isRequired
  };

  componentWillMount () {
    if (isEmpty(this.props.cards)) {
      this.fuse = new Fuse();
    } else {
      this.fuse = this.getFuse(this.props);
    }
    this.setQuery = debounce(query => {
      this.props.setQuery(query);
    }, 300); // eslint-disable-line no-magic-numbers
  }

  componentWillReceiveProps (nextProps) {
    // see if fuse index needs to be rebuilt
    if (
      !isMatch(this.props.cards, nextProps.cards) ||
      !isMatch(this.props.filter, nextProps.filter) ||
      (isMatch(this.props.cards, nextProps.cards) &&
        isEmpty(nextProps.filter) &&
        this.props.filter !== nextProps.filter)
    ) {
      this.fuse = this.getFuse(nextProps);
    }
  }

  filterCards (props) {
    const sets = flow(
      filter({ type: "set" }),
      map(value => value.name)
    )(props.filter);
    const affinities = flow(
      filter({ type: "affinity" }),
      map(value => value.value)
    )(props.filter);
    return filter(card => {
      let inSet = isEmpty(sets) || includes(card.set)(sets);
      let hasAffinities =
        isEmpty(affinities) ||
        intersection(affinities, card.affinities).length === affinities.length;

      return inSet && hasAffinities;
    })(props.cards);
  }

  getFuse (props) {
    // massage the selection
    let cards = this.filterCards(props).map(card => ({
      ...card,
      label: `${card.name}, ${card.title}`
    }));
    return new Fuse(cards, { keys: ["label"], threshold: 0.3 });
  }

  render () {
    // massage the selection
    const { properties, orders } = this.props.sort;
    let cards = this.filterCards(this.props);
    cards = orderBy(properties, orders)(
      isEmpty(this.props.query) ? cards : this.fuse.search(this.props.query)
    );

    let cardIds = map("_id")(cards);

    /* eslint-disable no-magic-numbers */
    return (
      <div styleName="card-search">
        <CardSearchFilterBar
          filter={this.props.filter}
          setFilter={this.props.setFilter}
          query={this.props.query}
          setQuery={this.setQuery}
          sort={this.props.sort}
          setSort={this.props.setSort}
        />
        {!isEmpty(this.props.cards) ? (
          <div styleName="card-list-wrapper">
            <AutoSizer>
              {({ width, height }) => {
                let columnWidth = 166,
                    rowHeight = 226,
                    numColumns = Math.floor(width / columnWidth),
                    numRows = Math.ceil(cards.length / numColumns);
                return (
                  <Grid
                    width={width}
                    height={height}
                    cellRenderer={({ columnIndex, rowIndex }) => {
                      let index = columnIndex + rowIndex * numColumns;
                      let card = cards[index];
                      if (card) {
                        // TODO: Get in collection.
                        let inCollection = 0;

                        return (
                          <div
                            className={styles["card-wrapper"]}
                            onClick={() => {
                              this.props.setRow(rowIndex);
                              this.context.router.push(
                                `card/${encodeURIComponent(card._id)}`
                              );
                            }}
                          >
                            <CardThumbListItem
                              key={card._id}
                              card={card}
                              inCollection={inCollection}
                            />
                          </div>
                        );
                      }
                      return null;
                    }}
                    scrollToRow={this.props.row}
                    rowCount={numRows}
                    columnCount={numColumns}
                    rowHeight={rowHeight}
                    columnWidth={columnWidth}
                    noContentRenderer={() => (
                      <div className={styles["message-wrapper"]}>
                        <Paper className={styles["message-card"]}>
                          <Search style={{ width: "3rem", height: "3rem" }} />
                          <div>No Results Found</div>
                        </Paper>
                      </div>
                    )}
                  />
                );
              }}
            </AutoSizer>
          </div>
        ) : (
          <div styleName="message-wrapper">
            <CircularProgress size={5} />
          </div>
        )}
      </div>
    );
    /* eslint-enable */
  }
}

export default connect(
  state => ({
    filter: state.CardSearch.filter,
    query: state.CardSearch.query,
    row: state.CardSearch.row,
    sort: state.CardSearch.sort,
    cards: state.App.cards
  }),
  dispatch => ({
    setFilter: filter => dispatch(setFilter(filter)),
    setQuery: query => dispatch(setQuery(query)),
    setRow: row => dispatch(setRow(row)),
    setSort: sort => dispatch(setSort(sort))
  })
)(CardSearch);
