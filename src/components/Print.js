import React, { Component } from "react";
import { connect } from "react-redux";

import { printSelector } from "../reducers/App";
import { getCards } from "../actions/App";

import fireEmblem from "../resources/fire-emblem.svg";

const Card = ({
  promotion,
  name,
  title,
  series,
  parsedSkills,
  setIcon,
  cost,
  thumbnail
}) => {
  const identifier = `${name}, ${title} (${series})`;
  return (
    <div className="card" key={identifier}>
      <div className="meta">
        <img className="icon" src={setIcon} />
        <span className="name">{identifier}</span>
        <span className="affinities">
          Cost: {cost} {promotion && `(${promotion})`}
        </span>
      </div>
      <div className="content">
        <div className="thumbnail-wrapper">
          <img className="thumbnail" src={thumbnail} />
        </div>
        <div className="info">
          <div className="skills">
            {parsedSkills.map(skill => (
              <div
                className="skill"
                key={skill}
                dangerouslySetInnerHTML={{ __html: skill }}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

class Print extends Component {
  componentWillMount () {
    this.props.getCards();
  }
  render () {
    const {
      print: [mainCharacterCards, cards]
    } = this.props;
    return (
      <div className="print-wrapper">
        <style>
          {`.print-wrapper {
            }
            .main-character .info {
              background-image: url(${fireEmblem});
              background-repeat: round;
              -webkit-print-color-adjust: exact;
            }
            .card {
              display: inline-block;
              width: 49%;
              page-break-inside: avoid;
              font-size: 10px;
              border: solid 1px #000;
            }
            .content {
              display: flex;
            }
            .thumbnail-wrapper, .info {
              display: inline-block;
            }
            .thumbnail-wrapper {
              display: flex;
            }
            .thumbnail {
              height: 150px;
            }
            .info {
              border-left: solid 1px #000;
              flex: 1;
            }
            .meta {
              padding: .5em;
              border-bottom: solid 1px #000;
            }
            .affinities {
              float: right;
            }
            .skills {
              padding: .5em;
            }
            .skill {
              margin-bottom: .5em;
            }
            .skill:last-child {
              margin-bottom: initial;
            }
            .skill-icon, .icon, .skill-type-icon {
              height: 1em;
              vertical-align: text-top;
            }
          `}
        </style>
        <div className="main-character">{mainCharacterCards.map(Card)}</div>
        {cards.map(Card)}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  print: printSelector(state)
});

const mapDispatchToProps = dispatch => ({
  getCards: doUpdate => dispatch(getCards(doUpdate))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Print);
