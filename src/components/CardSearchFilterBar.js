import React, {PropTypes} from 'react';
import CSSModules from 'react-css-modules';

import isEmpty from 'lodash/fp/isEmpty';

import {MultiSelect} from 'react-selectize';
import TextField from 'material-ui/TextField';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import IconMenu from 'material-ui/IconMenu';
import Sort from 'material-ui/svg-icons/content/sort';

import CardIconUtils from '../services/cardIconUtils';

import styles from './CardSearchFilterBar.pcss';

@CSSModules(styles)
class CardSearchFilterBar extends React.Component {
  static propTypes = {
    filter: PropTypes.array.isRequired,
    query: PropTypes.string.isRequired,
    setFilter: PropTypes.func.isRequired,
    setQuery: PropTypes.func.isRequired,
    sort: PropTypes.object.isRequired,
    setSort: PropTypes.func.isRequired
  };

  componentWillMount () {
    this.setState({
      searchQuery: this.props.query || ''
    });
  }

  render () {
    const sorts = [{
      name: 'Series (Ascending)',
      properties: ['series'],
      orders: ['asc']
    }, {
      name: 'Series (Descending)',
      properties: ['series'],
      orders: ['desc']
    }, {
      name: 'Name (A-Z)',
      properties: ['name', 'title'],
      orders: ['asc']
    }, {
      name: 'Name (Z-A)',
      properties: ['name', 'title'],
      orders: ['desc', 'desc']
    }, {
      name: 'Cost (Ascending)',
      properties: ['cost', 'promotion', 'name', 'title'],
      orders: ['asc']
    }, {
      name: 'Cost (Descending)',
      properties: ['cost', 'promotion', 'name', 'title'],
      orders: ['desc', 'desc', 'asc', 'asc']
    }, {
      name: 'Attack (Ascending)',
      properties: ['attack', 'name', 'title'],
      orders: ['asc']
    }, {
      name: 'Attack (Descending)',
      properties: ['attack', 'name', 'title'],
      orders: ['desc', 'asc', 'asc']
    }, {
      name: 'Support (Ascending)',
      properties: ['support', 'name', 'title'],
      orders: ['asc']
    }, {
      name: 'Support (Descending)',
      properties: ['support', 'name', 'title'],
      orders: ['desc', 'asc', 'asc']
    }];

    return (
      <div>
        <Toolbar>
          <ToolbarGroup styleName="search-group">
            <TextField
              fullWidth
              underlineStyle={{borderColor: 'rgba(0,0,0,0.3)'}}
              value={this.state.searchQuery}
              hintText="Search"
              onChange={event => {
                this.props.setQuery(event.target.value);
                this.setState({searchQuery: event.target.value});
              }}
            />
            {!isEmpty(this.state.searchQuery) ?
              <span styleName="input-clear-button"
                onClick={() => {
                  this.props.setQuery('');
                  this.setState({searchQuery: ''});
                }}
              >
                <svg styleName="input-clear-icon" focusable="false">
                  <path d="M0 0 L8 8 M8 0 L 0 8" />
                </svg>
              </span>
            : null}
          </ToolbarGroup>
          <ToolbarGroup styleName="toolbar-group">
            <MultiSelect className="card-search-select-box"
              values={this.props.filter}
              options={CardIconUtils.options}
              maxValues={10}
              theme="material"
              placeholder="Set & Affinities Filter"
              onValuesChange={values => this.props.setFilter(values)}
              renderOption={item => (
                <div className="simple-option">
                  <img className={styles['option-icon']} src={item.icon} />
                  <span>{item.label}</span>
                </div>
              )}
              renderValue={item => (
                <div>
                  <img className={styles['value-icon']} src={item.icon} />
                </div>
              )}
            />
            <IconMenu
              style={{marginLeft: '1rem'}}
              iconButtonElement={
                <FlatButton
                  icon={<Sort />}
                  label="sort"
                />
              }
              onChange={(event, value) => this.props.setSort(value)}
            >
              {sorts.map(({properties, orders, name}) => (
                <MenuItem key={name} value={{properties, orders}} primaryText={name} />
              ))}
            </IconMenu>
          </ToolbarGroup>
        </Toolbar>
      </div>
    );
  }
}

export default CardSearchFilterBar;
