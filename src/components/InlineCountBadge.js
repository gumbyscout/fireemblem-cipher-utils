import React, {PropTypes} from 'react';

class InlineCountBadge extends React.Component {
  static propTypes = {
    count: PropTypes.number.isRequired,
    noBackground: PropTypes.bool,
    className: PropTypes.string,
    style: PropTypes.object
  }

  static contextTypes = {
    muiTheme: PropTypes.object.isRequired
  }

  styles () {
    return {
      badge: {
        borderRadius: '50%',
        color: '#fff',
        backgroundColor: this.context.muiTheme.rawTheme.palette.accent1Color,
        width: '24px',
        height: '24px',
        fontWeight: '500',
        fontSize: '12px',
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexFlow: 'row wrap'
      },
      noBackground: {
        backgroundColor: 'initial',
        color: '#000'
      }
    };
  }

  render () {
    let style = this.props.noBackground ? {...this.styles().badge, ...this.styles().noBackground} : this.styles().badge;
    return (
      <span style={{...this.props.style, ...style}} className={this.props.className}>
        {this.props.count}
      </span>
    );
  }
}

export default InlineCountBadge;
