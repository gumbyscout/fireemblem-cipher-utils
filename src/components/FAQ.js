import React from 'react';
import CSSModules from 'react-css-modules';

import Paper from 'material-ui/Paper';

import ExternalLink from './ExternalLink';

import styles from './FAQ.pcss';

@CSSModules(styles)
class FAQ extends React.Component {
  render () {
    return (
      <div styleName="wrapper">
        <Paper>
          <div styleName="page">
          <h3>FAQ</h3>
          <h4>Q. What is Fire Emblem 0 (Cipher)?</h4>
          <p>
            A. Fire Emblem 0 (Cipher) is a trading card game developed by Intelligent Systems, and published by Nintendo. The game features various characters from the Fire Emblem series. The game officially debuted on June 25, 2015 with the release of Fire Emblem Fates in Japan.
          </p>
          <h4>Q. What is this program?</h4>
          <p>
            A. Fire Emblem Cipher Utils is a desktop application that serves as a card database &amp; manager for the Fire Emblem 0 (Cipher) trading card game. Since the game is only in Japanese, it targets English speaking players that want to play the game as well.
          </p>
          <h4>Q. What are some of the features of this program?</h4>
          <p>A. The application is ever-growing, but as of now this application has:</p>
          <ul>
            <li>Translations and card lookup for all available cards.</li>
            <li>Deck creation, with the ability to import and export for deck sharing.</li>
            <li>Cheat sheet creation.</li>
            <li>Collection management to keep track of what cards you own.</li>
          </ul>
          <h4>Q. How do you play Fire Emblem 0 (Cipher)?</h4>
          <p>
            A. The goal of the game is to defeat the opponent’s Main Character by using your own units to attack. There are plenty of resources available that explain the rules in detail. One example is&nbsp;
            <ExternalLink
              link="https://serenesforest.net/wiki/index.php/Comprehensive_Rules_of_FE_Cipher"
            >Comprehensive Rules of FE Cipher</ExternalLink>.
          </p>
          <h4>Q. What are some things I should know when using this program?</h4>
          <p>
            A. This application assumes you have a basic understanding of how the game is played. But some things to remember are:
          </p>
          <ul>
            <li>Normal cards are only allowed, at most, four copies in a deck.</li>
            <li>There are exceptions, such as Mysterious Shop-keep, Anna, that allows a player to exceed the normal 4-per-deck limit.</li>
            <li>A deck must have at least 50 cards. There is no maximum.</li>
            <li>When the game starts, your beginning Main Character must be Cost 1.</li>
          </ul>
          <h4>Q. I can't add cards my deck. What's going on?</h4>
          <p>
            A. This application requires a deck template be created first in order for cards to be added. New decks can be created by going to the "DECK" section, and then clicking the Green + button. This should create a new blank deck template. Click the template to make it the active deck. You should be able to rename the deck, select the Main Character, and even add cards.
          </p>
          <h4>Q. How can I declare a Main Character in my deck?</h4>
          <p>
            A. Under DECKS, make sure the deck to edit the current deck, then click on EDIT CURRENT DECK. To select a Main Character, open the drop down box and select from a list of eligible card in the deck. Be sure to save the changes, and you should see the Main Character being identified in the deck drawer on the left, as well as the printout of a cheat sheet.
          </p>
          <h4>Q. How do I manually update the database? I’m missing some cards!</h4>
          <p>
            A. This application can access a remote database where all the cards are pulled from, and stores them on your local machine to use offline. Under SETTINGS, click on the button UPDATE DB. (Note: You MUST be connected to the Internet to update)
          </p>
          <h4>Q. I found an issue!</h4>
          <p>
            A. Thank Ashera! Please let us know by reporting issues&nbsp;
            <ExternalLink
              link="https://github.com/gumbyscout/fireemblem-cipher-utils/issues"
            >here</ExternalLink>
          </p>
          <h3>Credits &amp; Sources</h3>
          <p>
            <ExternalLink
              link="http://serenesforest.net/"
            >SerenesForest.net</ExternalLink>– Providing valuable card translations and for being a great Fire Emblem resource
          </p>
          <p>
            Nintendo &amp; Intelligent Systems – For making a series we have come to love!
          </p>
          </div>
        </Paper>
      </div>
    );
  }
}

export default FAQ;
