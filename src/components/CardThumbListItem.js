import React, {PropTypes} from 'react';
import CSSModules from 'react-css-modules';

import Work from 'material-ui/svg-icons/action/work';
import {Card, CardMedia, CardTitle} from 'material-ui/Card';

import CardIconUtils from '../services/cardIconUtils';

import styles from './CardThumbListItem.pcss';

@CSSModules(styles)
class CardThumbListItem extends React.Component {
  static propTypes = {
    card: PropTypes.shape({
      name: PropTypes.string.isRequired,
      imageUrl: PropTypes.string.isRequired,
      '_attachments': PropTypes.object,
      title: PropTypes.string
    }).isRequired,
    inCollection: PropTypes.number
  }

  render () {
    const {name, title} = this.props.card;
    const imageSrc = CardIconUtils.getThumbnail(this.props.card);
    const iconStyle = {
      height: '20px',
      width: '20px'
    };
    return (
      <Card styleName="card">
        <CardMedia
          overlay={
            <CardTitle
              title={`${name}, ${title}`}
              titleStyle={{
                fontSize: '1rem',
                lineHeight: '1rem'
              }}
            />
          }
        >
          <img src={imageSrc}/>
        </CardMedia>
        {this.props.inCollection ?
          <span styleName="icon-wrapper">
            <Work color="#fff" style={iconStyle} />
          </span>
        : null}
      </Card>
    );
  }
}

export default CardThumbListItem;
