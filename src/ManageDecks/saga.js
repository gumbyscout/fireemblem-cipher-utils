import { takeEvery, takeLatest, select, call, put } from "redux-saga/effects";

import {
  UPDATE_CURRENT_DECK,
  GET_DECKS,
  SAVE_DECK,
  DELETE_DECK,
  SET_CURRENT_DECK,
  currentDeckSelector,
  setCurrentDeck,
  getDecksSuccess,
  getDecksFailure,
  saveDeckFailure,
  saveDeckSuccess,
  deleteDeckSuccess,
  deleteDeckFailure
} from "./reducer";

import DeckDatabase from "./db";

export function* updateCurrentDeckSaga ({ payload: updater }) {
  const { _id } = yield select(currentDeckSelector);
  try {
    const newDeck = yield call([DeckDatabase, "updateDeck"], _id, updater);
    yield put(saveDeckSuccess(newDeck));
    yield put(setCurrentDeck(newDeck));
  } catch (e) {
    yield put(saveDeckFailure(e));
  }
}

export function* getDecksSaga () {
  try {
    const decks = yield call([DeckDatabase, "getDecks"]);
    yield put(getDecksSuccess(decks));
  } catch (e) {
    yield put(getDecksFailure(e));
  }
}

export function* saveDeckSaga ({ payload: deck }) {
  try {
    const updatedDeck = yield call([DeckDatabase, "saveDeck"], deck);
    yield put(saveDeckSuccess(updatedDeck));
  } catch (e) {
    yield put(saveDeckFailure(e));
  }
}

export function* deleteDeckSaga ({ payload: deck }) {
  if (window.confirm(`Are you sure you want to delete ${deck.name}?`)) {
    try {
      yield call([DeckDatabase, "deleteDeck"], deck);
      yield put(deleteDeckSuccess(deck._id));
      yield setCurrentDeckSaga({});
    } catch (e) {
      yield put(deleteDeckFailure(e));
    }
  }
}

export function* setCurrentDeckSaga ({ payload: deck }) {
  const CURRENT_DECK_KEY = "currentDeck";
  if (!deck) {
    yield call([localStorage, "removeItem"], CURRENT_DECK_KEY);
  } else {
    yield call(
      [localStorage, "setItem"],
      CURRENT_DECK_KEY,
      JSON.stringify(deck)
    );
  }
}

export default function*() {
  yield takeEvery(UPDATE_CURRENT_DECK, updateCurrentDeckSaga);
  yield takeLatest(GET_DECKS, getDecksSaga);
  yield takeLatest(SAVE_DECK, saveDeckSaga);
  yield takeLatest(DELETE_DECK, deleteDeckSaga);
  yield takeEvery(SET_CURRENT_DECK, setCurrentDeckSaga);
}
