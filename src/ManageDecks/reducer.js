import { handleActions, createAction } from "redux-actions";
import { createSelector } from "reselect";
import { flow, reject, sortBy, get, remove } from "lodash/fp";

import { upsertInArray } from "../utils";

/*
 █████   ██████ ████████ ██  ██████  ███    ██ ███████
██   ██ ██         ██    ██ ██    ██ ████   ██ ██
███████ ██         ██    ██ ██    ██ ██ ██  ██ ███████
██   ██ ██         ██    ██ ██    ██ ██  ██ ██      ██
██   ██  ██████    ██    ██  ██████  ██   ████ ███████
*/

export const SET_CURRENT_DECK = "SET_CURRENT_DECK";
export const GET_DECKS = "GET_DECKS";
export const GET_DECKS_SUCCESS = "GET_DECKS_SUCCESS";
export const GET_DECKS_FAILURE = "GET_DECKS_FAILURE";
export const EDIT_DECK = "EDIT_DECK";
export const SAVE_DECK = "SAVE_DECK";
export const SAVE_DECK_SUCCESS = "SAVE_DECK_SUCCESS";
export const SAVE_DECK_FAILURE = "SAVE_DECK_FAILURE";
export const DELETE_DECK = "DELETE_DECK";
export const DELETE_DECK_SUCCESS = "DELETE_DECK_SUCCESS";
export const DELETE_DECK_FAILURE = "DELETE_DECK_FAILURE";

export const UPDATE_CURRENT_DECK = "UPDATE_CURRENT_DECK";

export const editDeck = createAction(EDIT_DECK);

export const saveDeck = createAction(SAVE_DECK);
export const saveDeckSuccess = createAction(SAVE_DECK_SUCCESS);
export const saveDeckFailure = createAction(SAVE_DECK_FAILURE);

export const deleteDeck = createAction(DELETE_DECK);
export const deleteDeckSuccess = createAction(DELETE_DECK_SUCCESS);
export const deleteDeckFailure = createAction(DELETE_DECK_FAILURE);

export const getDecks = createAction(GET_DECKS);
export const getDecksSuccess = createAction(GET_DECKS_SUCCESS);
export const getDecksFailure = createAction(GET_DECKS_FAILURE);

export const setCurrentDeck = createAction(SET_CURRENT_DECK);

export const updateCurrentDeck = createAction(UPDATE_CURRENT_DECK);

/*
██████  ███████ ██████  ██    ██  ██████ ███████ ██████
██   ██ ██      ██   ██ ██    ██ ██      ██      ██   ██
██████  █████   ██   ██ ██    ██ ██      █████   ██████
██   ██ ██      ██   ██ ██    ██ ██      ██      ██   ██
██   ██ ███████ ██████   ██████   ██████ ███████ ██   ██
*/

const seedCurrentDeck = () => {
  try {
    return JSON.parse(localStorage.getItem("currentDeck")) || {};
  } catch (e) {
    return {};
  }
};

const initialState = {
  decks: [],
  loading: true,
  error: null,
  currentDeck: seedCurrentDeck()
};

export default handleActions(
  {
    [GET_DECKS_SUCCESS]: (state, { payload: decks }) => ({
      ...state,
      decks,
      loading: false
    }),
    [GET_DECKS_FAILURE]: (state, { payload: error }) => ({
      ...state,
      loading: false,
      error
    }),
    [SET_CURRENT_DECK]: (state, { payload: currentDeck }) => ({
      ...state,
      currentDeck
    }),
    [SAVE_DECK_SUCCESS]: (state, { payload: deck }) => ({
      ...state,
      decks: upsertInArray({ _id: deck.id })(deck)(state.decks)
    }),
    [DELETE_DECK_SUCCESS]: (state, { payload: _id }) => ({
      ...state,
      decks: remove({ _id }, state.decks),
      currentDeck: {}
    })
  },
  initialState
);

/*
███████ ███████ ██      ███████  ██████ ████████  ██████  ██████  ███████
██      ██      ██      ██      ██         ██    ██    ██ ██   ██ ██
███████ █████   ██      █████   ██         ██    ██    ██ ██████  ███████
     ██ ██      ██      ██      ██         ██    ██    ██ ██   ██      ██
███████ ███████ ███████ ███████  ██████    ██     ██████  ██   ██ ███████
*/

export const currentDeckSelector = get("ManageDecks.currentDeck");

export const decksSelector = get("ManageDecks.decks");

export const otherDecksSelector = createSelector(
  decksSelector,
  currentDeckSelector,
  (decks, { _id }) =>
    flow(
      reject({ _id }),
      sortBy("name")
    )(decks)
);
