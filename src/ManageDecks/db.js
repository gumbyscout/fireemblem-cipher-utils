import PouchDB from "pouchdb-browser";
PouchDB.plugin(require("pouchdb-upsert"));

/* eslint-disable no-console */
class DeckDatabase {
  constructor () {
    this.isReady = this.reinstantiate();
  }

  reinstantiate () {
    this.db = new PouchDB("fireemblem-cipher-collection");

    return this.db.info();
  }

  getDecks () {
    return this.isReady.then(() =>
      this.db
        .allDocs({ include_docs: true })
        .then(results => results.rows.map(row => row.doc))
    );
  }

  getDeck (id) {
    return this.db.get(id);
  }

  saveDeck (deck) {
    return this.db.put(deck).then(() => this.db.get(deck._id));
  }

  updateDeck (_id, updater) {
    return this.db.upsert(_id, updater).then(({ id }) => this.db.get(id));
  }

  deleteDeck (deck) {
    return this.db.remove(deck);
  }
}

export default new DeckDatabase();
