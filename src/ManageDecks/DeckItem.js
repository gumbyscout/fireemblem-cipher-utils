import React from "react";
import PropTypes from "prop-types";

import { ListItem } from "material-ui/List";

import map from "lodash/fp/map";
import countBy from "lodash/fp/countBy";
import times from "lodash/fp/times";
import flow from "lodash/fp/flow";
import flatMap from "lodash/fp/flatMap";
import toPairs from "lodash/fp/toPairs";
import get from "lodash/fp/get";

import CardIconUtils from "../services/cardIconUtils";
import InlineCountBadge from "../components/InlineCountBadge";

import styles from "./DeckItem.pcss";

class DeckItem extends React.Component {
  render () {
    let setCounts = flow(
      toPairs,
      flatMap(([cardId, count]) => {
        let card = this.props.cardsMap[cardId];
        return times(() => get("set")(card))(count);
      }),
      countBy(item => item),
      toPairs,
      map(([set, count]) => (
        <span key={set}>
          <img
            src={CardIconUtils.getSetIcon(set)}
            className={styles["set-icon"]}
          />
          <InlineCountBadge count={count} />
        </span>
      ))
    )(this.props.deck.cards);
    return (
      <ListItem
        key={this.props.deck._id}
        onClick={() => this.props.setCurrentDeck(this.props.deck)}
        primaryText={
          <span className={styles["primary-text"]}>
            <span className={styles["deck-name"]}>{this.props.deck.name}</span>
            <span>{setCounts}</span>
          </span>
        }
      />
    );
  }
}

DeckItem.propTypes = {
  deck: PropTypes.object.isRequired,
  cardsMap: PropTypes.object.isRequired,
  setCurrentDeck: PropTypes.func.isRequired
};

export default DeckItem;
