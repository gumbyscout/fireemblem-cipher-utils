import { connect } from "react-redux";
import Chance from "chance";
import React from "react";
import PropTypes from "prop-types";
import FileSaver from "file-saver";

const chance = new Chance();

import { snakeCase, head, isEmpty, assign, pick } from "lodash/fp";

import { Toolbar, ToolbarGroup } from "material-ui/Toolbar";
import { List, ListItem } from "material-ui/List";
import FlatButton from "material-ui/FlatButton";
import Subheader from "material-ui/Subheader";
import FloatingActionButton from "material-ui/FloatingActionButton";
import ContentAdd from "material-ui/svg-icons/content/add";
import Divider from "material-ui/Divider";

import {
  saveDeck,
  setCurrentDeck,
  getDecks,
  otherDecksSelector,
  currentDeckSelector
} from "./reducer";
import DeckItem from "./DeckItem";
import { cardsMapSelector } from "../reducers/App";

const styles = {
  manageDecks: {
    display: "flex",
    flexDirection: "column",
    flex: "1",
    overflow: "hidden"
  },
  deckList: {
    overflow: "auto",
    flex: "1"
  },
  fab: {
    position: "absolute",
    bottom: "1.5rem",
    right: "1.5rem"
  }
};

const DECK_FILE_EXTENSION = "fecudk";

class ManageDecks extends React.Component {
  componentDidMount () {
    this.props.getDecks();
  }

  importDeck = () => {
    const { saveDeck } = this.props;
    const files = this.fileInput.files;

    let file = head(files);
    if (file) {
      const fileReader = new FileReader();
      fileReader.onload = ({ target: { result: data } }) => {
        let deck = assign(JSON.parse(data), {
          _id: chance.guid()
        });
        saveDeck(deck);
      };
      fileReader.onerror = e => {
        throw e;
      };
      fileReader.readAsText(file);
    }
  };

  exportDeck = () => {
    const { currentDeck } = this.props;
    const file = new Blob(
      [JSON.stringify(pick(["name", "cards"], currentDeck))],
      { type: "application/json" }
    );
    FileSaver.saveAs(
      file,
      `${snakeCase(currentDeck.name)}.${DECK_FILE_EXTENSION}`
    );
  };

  createDeck = () => {
    this.props.saveDeck({
      _id: chance.guid(),
      name: "New Deck",
      cards: {}
    });
  };

  render () {
    const { currentDeck, otherDecks, cardsMap, setCurrentDeck } = this.props;

    return (
      <div style={styles.manageDecks}>
        <Toolbar>
          <ToolbarGroup firstChild>
            <input
              type="file"
              style={{ display: "none" }}
              ref={ref => {
                this.fileInput = ref;
              }}
              onChange={this.importDeck}
              accept={`.${DECK_FILE_EXTENSION}`}
            />
            <FlatButton
              label="import deck"
              onClick={() => this.fileInput.click()}
            />
          </ToolbarGroup>
          <ToolbarGroup lastChild>
            <FlatButton label="Export Current Deck" onClick={this.exportDeck} />
          </ToolbarGroup>
        </Toolbar>
        <List style={styles.deckList}>
          {!isEmpty(currentDeck) || !isEmpty(otherDecks) ? (
            <span>
              <Subheader>Current Deck</Subheader>
              {!isEmpty(currentDeck) ? (
                <DeckItem
                  deck={currentDeck}
                  cardsMap={cardsMap}
                  setCurrentDeck={setCurrentDeck}
                />
              ) : (
                <ListItem>
                  Click on an existing deck to make it the current deck
                </ListItem>
              )}
              <Divider />
              <Subheader>Decks</Subheader>
              {!isEmpty(otherDecks) ? (
                otherDecks.map(deck => (
                  <DeckItem
                    key={deck._id}
                    deck={deck}
                    cardsMap={cardsMap}
                    setCurrentDeck={setCurrentDeck}
                  />
                ))
              ) : (
                <ListItem>No other decks currently</ListItem>
              )}
            </span>
          ) : (
            <ListItem>Create a new deck to get started</ListItem>
          )}
          <FloatingActionButton style={styles.fab} onClick={this.createDeck}>
            <ContentAdd />
          </FloatingActionButton>
        </List>
      </div>
    );
  }
}

ManageDecks.propTypes = {
  otherDecks: PropTypes.array.isRequired,
  cardsMap: PropTypes.object.isRequired,
  currentDeck: PropTypes.object.isRequired,
  saveDeck: PropTypes.func.isRequired,
  setCurrentDeck: PropTypes.func.isRequired,
  getDecks: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  cardsMap: cardsMapSelector(state),
  otherDecks: otherDecksSelector(state),
  currentDeck: currentDeckSelector(state)
});

const mapDispatchToProps = dispatch => ({
  saveDeck: deck => dispatch(saveDeck(deck)),
  getDecks: () => dispatch(getDecks()),
  setCurrentDeck: deck => dispatch(setCurrentDeck(deck))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageDecks);
