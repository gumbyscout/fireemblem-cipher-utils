import PouchDB from "pouchdb-browser";
PouchDB.plugin(require("pouchdb-upsert"));

/* eslint-disable camelcase */
class CardDatabase {
  constructor () {
    this.isReady = this.reinstantiate();
  }

  reinstantiate () {
    this.db = new PouchDB("fireemblem-cipher-cards");

    return this.db.info().then(({ doc_count }) => {
      if (doc_count === 0) {
        import("./db.json").then(({ cards }) => this.db.bulkDocs(cards));
      }
    });
  }

  getCards () {
    return this.db
      .allDocs({ include_docs: true })
      .then(({ rows }) => rows.map(row => row.doc));
  }

  getCard (id) {
    return this.db.get(id);
  }
}

export default new CardDatabase();
