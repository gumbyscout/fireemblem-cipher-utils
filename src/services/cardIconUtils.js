const forEach = require("lodash/forEach");
const template = require("lodash/template");

const flow = require("lodash/fp/flow");
const flatMap = require("lodash/fp/flatMap");
const map = require("lodash/fp/map");
const findKey = require("lodash/fp/findKey");
const pickBy = require("lodash/fp/pickBy");
const keys = require("lodash/fp/keys");
const assign = require("lodash/fp/assign");
const reduce = require("lodash/fp/reduce");
const reject = require("lodash/fp/reject");
const get = require("lodash/fp/get");

let cardback, icons, thumbnails;
if (process.env.NODE_ENV !== "TOOL") {
  cardback = require("../resources/cardback.jpg");
  icons = require("../resources/icons/*.png");
  thumbnails = require("../resources/images/*.webp");
} else {
  cardback = "";
  icons = [];
  thumbnails = [];
}

class CardIconUtils {
  static getAffinityIcon (affinity) {
    return icons[affinity];
  }

  static parseSkill (skill) {
    // make undefined an empty string
    skill = skill || "";

    // pull skill name out
    let title = "";
    let titleMatches = skill.match(/[\“\"][^\"\”]*[\"\”]/g);
    if (titleMatches) {
      title = titleMatches[0]; // eslint-disable-line no-magic-numbers
      skill = skill.replace(title, "");
    }

    // tokenize the string
    forEach(CardIconUtils.allTokens, (key, token) => {
      skill = skill.replace(
        new RegExp(`[^{("“]?\\b(${token})\\b[^})"”]`, "gi"),
        " ${" + key + "}"
      );
    });

    return template(`<b>${title}</b> ${skill}`)(CardIconUtils.iconElements);
  }

  static getThumbnail (card) {
    return flow(
      get("thumbnail"),
      thumbnail => get(thumbnail, thumbnails) || cardback
    )(card);
  }

  static getSetIcon (set) {
    let key = flow(
      pickBy({ type: "set" }),
      findKey(icon => icon.name === set)
    )(CardIconUtils.icons);
    return icons[key];
  }
}

CardIconUtils.icons = {
  cont: {
    name: "Continuous",
    type: "skill",
    tokens: ["cont"]
  },
  act: {
    name: "Activate",
    type: "skill",
    tokens: ["act"]
  },
  auto: {
    name: "Trigger",
    type: "skill",
    tokens: ["auto"]
  },
  spec: {
    name: "Special",
    type: "skill",
    tokens: ["spec", "special"]
  },
  bond: {
    name: "Bond",
    type: "skill",
    tokens: ["bond skill"]
  },
  cf: {
    name: "Carnage Form",
    type: "skill",
    tokens: ["carnage form", "cf"]
  },
  ccs: {
    name: "Class Change Skill",
    type: "skill",
    tokens: ["class change skill", "ccs.png"]
  },
  fs: {
    name: "Formation Skill",
    type: "skill",
    tokens: ["formation skill", "fs"]
  },
  bs: {
    name: "Blood Skill",
    type: "skill",
    tokens: ["bs"]
  },
  us: {
    name: "Union Skill",
    type: "skill",
    tokens: ["union skill"]
  },
  dv: {
    name: "Dragon Vein",
    type: "skill",
    tokens: ["dragon vein"]
  },
  level2: {
    name: "Level 2 Skill",
    type: "skill",
    tokens: ["level skill 2"]
  },
  level3: {
    name: "Level 3 Skill",
    type: "skill",
    tokens: ["level skill 3"]
  },
  level4: {
    name: "Level 4 Skill",
    type: "skill",
    tokens: ["level skill 4"]
  },
  level5: {
    name: "Level 5 Skill",
    type: "skill",
    tokens: ["level skill 5"]
  },
  once_per_turn: {
    name: "Once Per Turn",
    type: "action",
    tokens: ["once per turn"]
  },
  tap: {
    name: "Tap this unit",
    type: "action",
    tokens: ["tap this unit", "tap.png"]
  },
  flip1: {
    name: "Flip 1 bond",
    type: "action",
    tokens: ["flip 1 bonds?"]
  },
  flip2: {
    name: "Flip 2 bonds",
    type: "action",
    tokens: ["flip 2 bonds?"]
  },
  flip3: {
    name: "Flip 3 bonds",
    type: "action",
    tokens: ["flip 3 bonds?"]
  },
  flip4: {
    name: "Flip 4 bonds",
    type: "action",
    tokens: ["flip 4 bonds?"]
  },
  flip5: {
    name: "Flip 5 bonds",
    type: "action",
    tokens: ["flip 5 bonds?"]
  },
  atk_supp: {
    name: "Attack Support",
    type: "skill-png",
    tokens: ["Attack Support.png"]
  },
  def_supp: {
    name: "Defense Support",
    type: "skill-png",
    tokens: ["Defense Support.png"]
  },
  atk_def: {
    name: "Attack / Defence Support",
    type: "skill-png",
    tokens: ["Attack / Defence Support"]
  },
  black: {
    name: "Nohr",
    type: "set",
    tokens: ["black"]
  },
  red: {
    name: "Sword of Light",
    type: "set",
    tokens: ["red"]
  },
  white: {
    name: "Hoshido",
    type: "set",
    tokens: ["white"]
  },
  blue: {
    name: "Mark of Naga",
    type: "set",
    tokens: ["blue"]
  },
  green: {
    name: "Medallion",
    type: "set",
    tokens: ["green"]
  },
  purple: {
    name: "Divine Weapons",
    type: "set",
    tokens: ["purple"]
  },
  yellow: {
    name: "Holy War Flag",
    type: "set",
    tokens: ["yellow"]
  },
  sharp: {
    name: "Tokyo Mirage Sessions",
    type: "affinity",
    tokens: ["tms", "genei", "#fe", "sharp"]
  },
  armor: {
    name: "Armored",
    type: "affinity",
    tokens: ["armored", "armor"]
  },
  axe: {
    name: "Axe",
    type: "affinity",
    tokens: ["axe"]
  },
  bow: {
    name: "Bow",
    type: "affinity",
    tokens: ["bow"]
  },
  fang: {
    name: "Fang",
    type: "affinity",
    tokens: ["fang", "fangs"]
  },
  female: {
    name: "Female",
    type: "affinity",
    tokens: ["female"]
  },
  flying: {
    name: "Flying",
    type: "affinity",
    tokens: ["flying"]
  },
  horse: {
    name: "Mounted",
    type: "affinity",
    tokens: ["horse", "mounted", "mount"]
  },
  lance: {
    name: "Lance",
    type: "affinity",
    tokens: ["lance"]
  },
  male: {
    name: "Male",
    type: "affinity",
    tokens: ["male"]
  },
  shuriken: {
    name: "Shuriken",
    type: "affinity",
    tokens: ["shuriken", "kunai"]
  },
  staff: {
    name: "Staff",
    type: "affinity",
    tokens: ["staff"]
  },
  stone: {
    name: "Transformation Stone",
    type: "affinity",
    tokens: ["dragon stone", "stone"]
  },
  sword: {
    name: "Sword",
    type: "affinity",
    tokens: ["sword", "katana"]
  },
  dragon: {
    name: "Dragon",
    type: "affinity",
    tokens: ["dragon"]
  },
  tome: {
    name: "Tome",
    type: "affinity",
    tokens: ["tome"]
  }
};

CardIconUtils.allTokens = flow(
  keys,
  flatMap(key =>
    map(token => ({ [token]: key }))(CardIconUtils.icons[key].tokens)
  ),
  reduce((result, value) => assign(result, value), {})
)(CardIconUtils.icons);

if (process.env.NODE_ENV !== "TOOL") {
  CardIconUtils.iconElements = flow(
    keys,
    map(key => {
      let icon = CardIconUtils.icons[key];
      let replacement = "";
      let src = icons[key];
      replacement = `<img src="${src}" class="skill-icon" data-tip="${
        icon.name
      }" />`;
      return { [key]: replacement };
    }),
    reduce((result, value) => assign(result, value), {})
  )(CardIconUtils.icons);
}

CardIconUtils.options = flow(
  keys,
  map(key =>
    assign({
      label: CardIconUtils.icons[key].name,
      value: key,
      icon: icons[key]
    })(CardIconUtils.icons[key])
  ),
  reject(
    icon =>
      icon.type === "skill" ||
      icon.type === "skill-png" ||
      icon.type === "action"
  )
)(CardIconUtils.icons);

module.exports = CardIconUtils;
