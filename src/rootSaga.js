import { all } from "redux-saga/effects";

import { saga as cardDetail } from "./CardDetail";
import { saga as collection } from "./Collection";
import { saga as manageDecks } from "./ManageDecks";

function* rootSaga () {
  yield all([manageDecks(), cardDetail(), collection()]);
}

export default rootSaga;
