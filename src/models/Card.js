const toIndexableString = require('pouchdb-collate').toIndexableString;

const assign = require('lodash/assign');
const split = require('lodash/split');

const concat = require('lodash/fp/concat');
const without = require('lodash/fp/without');
const values = require('lodash/fp/values');
const pick = require('lodash/fp/pick');
const flow = require('lodash/fp/flow');
const map = require('lodash/fp/map');
const replace = require('lodash/fp/replace');
const pickBy = require('lodash/fp/pickBy');
const findKey = require('lodash/fp/findKey');
const includes = require('lodash/fp/includes');
const compact = require('lodash/fp/compact');

const CardIconUtils = require('../services/cardIconUtils');

/* eslint-disable no-magic-numbers */
class Card {
  constructor (card) {
    assign(this, card);
  }

  static fromLackeyCCGCard (lackeyCCGCard) {
    let names = split(lackeyCCGCard.name, ', ');
    return assign(new Card(), {
      lackeyId: lackeyCCGCard.ImageFile,
      lackeyName: names[0],
      lackeyTitle: names[1],
      lackeySkills: lackeyCCGCard.Skills,
      nameMatcher: lackeyCCGCard.name
    });
  }

  static fromSerenesWikiCard (serenesWikiCard) {
    let names = split(serenesWikiCard.name, ', ');
    let affinities = split(serenesWikiCard.other, ', ');
    let _id = toIndexableString([serenesWikiCard.series, names[1], names[0]]).replace(/\u0000/g, '\u0001');
    return assign(new Card(), {
      _id,
      series: serenesWikiCard.series,
      name: names[1],
      title: names[0],
      set: Card.insigniaToSet(serenesWikiCard.insignia),
      imageUrl: serenesWikiCard.imageUrl,
      class: serenesWikiCard.class,
      range: serenesWikiCard.range,
      cost: serenesWikiCard.cost,
      promotion: serenesWikiCard.promotion,
      attack: serenesWikiCard.attack,
      support: serenesWikiCard.support,
      illustrator: serenesWikiCard.illustrator,
      quote: serenesWikiCard.quote,
      affinities: flow(
        pick(['weapon', 'gender']),
        values,
        concat(affinities),
        without(['N/A', 'none', 'None', 'Pothead', 'Fat']),
        map(affinity => flow(
          pickBy({type: 'affinity'}),
          findKey(icon => includes(affinity.toLowerCase())(icon.tokens))
        )(CardIconUtils.icons))
      )(serenesWikiCard),
      skills: flow(
        pick([
          'skill1', 'skill2', 'skill3', 'skill4', 'supportSkill',
          'supportSkill1', 'supportSkill2', 'supportSkill3', 'supportSkill4'
        ]),
        values,
        compact,
        without(['N/A', 'undefined']),
        map(replace(/\n/g, ' '))
      )(serenesWikiCard),
      scrapedData: serenesWikiCard
    });
  }

  static insigniaToSet (insignia) {
    switch(insignia) {
    case 'Awakening (Blue)':
    case 'Mark of Naga (Blue)':
      insignia = 'Mark of Naga';
      break;
    case 'Nohr (Black)':
      insignia = 'Nohr';
      break;
    case 'Divine Weapons (Purple)':
    case 'Elibe (Purple)':
      insignia = 'Divine Weapons';
      break;
    }
    return insignia;
  }
}

module.exports = Card;
