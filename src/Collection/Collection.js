import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import CSSModules from "react-css-modules";
import { withRouter } from "react-router";

import { map, isEmpty, flow } from "lodash/fp";
const mapUncapped = map.convert({ cap: false });

import FlatButton from "material-ui/FlatButton";
import { Toolbar, ToolbarGroup, ToolbarTitle } from "material-ui/Toolbar";
import Avatar from "material-ui/Avatar";
import { List, ListItem } from "material-ui/List";
import Subheader from "material-ui/Subheader";
import Divider from "material-ui/Divider";

import InlineCountBadge from "../components/InlineCountBadge";

import CardIconUtils from "../services/cardIconUtils";
import {
  groupedCollectionSelector,
  collectionCountSelector,
  getCollection,
  exportCollection
} from "./reducer";

import styles from "./Collection.pcss";

@CSSModules(styles)
class Collection extends React.Component {
  componentDidMount () {
    this.props.getCollection();
  }
  importCollection = () => {
    // if (process.env.PLATFORM === "electron") {
    //   remote.dialog.showOpenDialog(
    //     {
    //       title: "Import Deck",
    //       filters: [
    //         {
    //           name: "FECU Collection",
    //           extensions: ["fecucl"]
    //         }
    //       ],
    //       properties: ["openFile"]
    //     },
    //     files => {
    //       let filePath = head(files);
    //       if (filePath) {
    //         fs.readFile(filePath, (error, data) => {
    //           if (error) {
    //             throw error;
    //           } else {
    //             // add to collection
    //             let collection = JSON.parse(data).collection;
    //             CollectionDatabase.mergeCollection(collection);
    //             getCollection();
    //           }
    //         });
    //       }
    //     }
    //   );
    // }
  };

  render () {
    const { groupedCards, collectionCount, exportCollection } = this.props;
    return (
      <div styleName="collection">
        <Toolbar>
          <ToolbarGroup firstChild>
            <FlatButton
              label="Import Collection"
              onClick={this.importCollection}
            />
            <FlatButton label="Export Collection" onClick={exportCollection} />
          </ToolbarGroup>
          <ToolbarGroup lastChild>
            <ToolbarTitle text={`Cards in collection ${collectionCount}`} />
          </ToolbarGroup>
        </Toolbar>
        <div styleName="content">
          {isEmpty(groupedCards) ? (
            <List>
              <ListItem primaryText="No cards in collection, add cards on card detail pages" />
            </List>
          ) : (
            mapUncapped((cardGroup, letter) => (
              <div key={letter}>
                <List>
                  <Subheader>{letter}</Subheader>
                  {cardGroup.map(card => (
                    <ListItem
                      leftAvatar={
                        <Avatar style={{ overflow: "hidden" }}>
                          <img
                            style={{ width: "inherit" }}
                            src={CardIconUtils.getThumbnail(card)}
                          />
                        </Avatar>
                      }
                      key={card._id}
                      primaryText={`${card.name}, ${card.title}`}
                      rightIcon={<InlineCountBadge count={card.count} />}
                      onClick={() => {
                        this.props.router.push(
                          `card/${encodeURIComponent(card._id)}`
                        );
                      }}
                    />
                  ))}
                </List>
                <Divider />
              </div>
            ))(groupedCards)
          )}
        </div>
      </div>
    );
  }
}

Collection.propTypes = {
  collectionCount: PropTypes.number,
  getCollection: PropTypes.func.isRequired,
  groupedCards: PropTypes.object,
  router: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  exportCollection: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  groupedCards: groupedCollectionSelector(state),
  collectionCount: collectionCountSelector(state)
});

const mapDispatchToProps = dispatch => ({
  getCollection: () => dispatch(getCollection()),
  exportCollection: () => dispatch(exportCollection())
});

export default flow(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter
)(Collection);
