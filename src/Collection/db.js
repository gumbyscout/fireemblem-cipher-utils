import PouchDB from "pouchdb-browser";
PouchDB.plugin(require("pouchdb-upsert"));

class CollectionDatabase {
  constructor () {
    this.isReady = this.reinstantiate();
  }

  reinstantiate () {
    this.db = new PouchDB("fireemblem-cipher-collection");

    return this.db.info();
  }

  getCollection () {
    return this.isReady.then(() =>
      this.db
        .allDocs({ include_docs: true }) // eslint-disable-line camelcase
        .then(({ rows }) => rows.map(row => row.doc))
    );
  }

  mergeCollection (collection) {
    this.db.bulkDocs(collection);
  }

  updateItem (id, updater) {
    return this.db.upsert(id, updater).then(() => this.getCollection());
  }
}

export default new CollectionDatabase();
