import { handleActions, createAction } from "redux-actions";
import { createSelector } from "reselect";
import {
  flow,
  filter,
  map,
  orderBy,
  compact,
  groupBy,
  sum,
  keyBy,
  assign,
  get
} from "lodash/fp";

import { cardsMapSelector } from "../reducers/App";

/*
 █████   ██████ ████████ ██  ██████  ███    ██ ███████
██   ██ ██         ██    ██ ██    ██ ████   ██ ██
███████ ██         ██    ██ ██    ██ ██ ██  ██ ███████
██   ██ ██         ██    ██ ██    ██ ██  ██ ██      ██
██   ██  ██████    ██    ██  ██████  ██   ████ ███████
*/

export const GET_COLLECTION = "GET_COLLECTION";
export const GET_COLLECTION_SUCCESS = "GET_COLLECTION_SUCCESS";
export const GET_COLLECTION_FAILURE = "GET_COLLECTION_FAILURE";
export const UPDATE_COLLECTION_ITEM = "UPDATE_COLLECTION_ITEM";
export const EXPORT_COLLECTION = "EXPORT_COLLECTION";

export const getCollection = createAction(GET_COLLECTION);
export const getCollectionSuccess = createAction(GET_COLLECTION_SUCCESS);
export const getCollectionFailure = createAction(GET_COLLECTION_FAILURE);
export const updateCollectionItem = createAction(UPDATE_COLLECTION_ITEM);
export const exportCollection = createAction(EXPORT_COLLECTION);

/*
██████  ███████ ██████  ██    ██  ██████ ███████ ██████
██   ██ ██      ██   ██ ██    ██ ██      ██      ██   ██
██████  █████   ██   ██ ██    ██ ██      █████   ██████
██   ██ ██      ██   ██ ██    ██ ██      ██      ██   ██
██   ██ ███████ ██████   ██████   ██████ ███████ ██   ██
*/

export const initialState = {
  loading: true,
  collection: [],
  error: null
};

export default handleActions(
  {
    [GET_COLLECTION]: (state, { payload }) => ({
      ...state,
      collection: payload
    }),
    [GET_COLLECTION_SUCCESS]: (state, { payload: collection }) => ({
      ...state,
      loading: false,
      collection
    }),
    [GET_COLLECTION_FAILURE]: (state, { payload: error }) => ({
      ...state,
      loading: false,
      error
    })
  },
  initialState
);

/*
███████ ███████ ██      ███████  ██████ ████████  ██████  ██████  ███████
██      ██      ██      ██      ██         ██    ██    ██ ██   ██ ██
███████ █████   ██      █████   ██         ██    ██    ██ ██████  ███████
     ██ ██      ██      ██      ██         ██    ██    ██ ██   ██      ██
███████ ███████ ███████ ███████  ██████    ██     ██████  ██   ██ ███████
*/

export const collectionSelector = get("Collection.collection");

export const groupedCollectionSelector = createSelector(
  cardsMapSelector,
  collectionSelector,
  (cardsMap, collection) =>
    flow(
      filter(item => item.count),
      map(item => assign(cardsMap[item._id], item)),
      orderBy(["name", "title"], ["asc"]),
      compact,
      groupBy(({ name = "" }) => name.charAt(0)) // eslint-disable-line no-magic-numbers
    )(collection)
);

export const collectionCountSelector = createSelector(
  collectionSelector,
  collection =>
    flow(
      map("count"),
      sum
    )(collection) || 0
);

export const collectionMapSelector = createSelector(
  collectionSelector,
  keyBy("_id")
);
