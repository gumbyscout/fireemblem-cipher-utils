import { takeEvery, call, put, takeLatest, select } from "redux-saga/effects";
import FileSaver from "file-saver";
import { snakeCase } from "lodash/fp";
import moment from "moment";

import {
  GET_COLLECTION,
  UPDATE_COLLECTION_ITEM,
  EXPORT_COLLECTION,
  getCollectionSuccess,
  getCollectionFailure,
  collectionSelector
} from "./reducer";
import DB from "./db";

const COLLECTION_FILE_EXTENSION = "fecucl";

export function* getCollectionSaga () {
  try {
    const collection = yield call([DB, "getCollection"]);
    yield put(getCollectionSuccess(collection));
  } catch (e) {
    yield put(getCollectionFailure(e));
  }
}

export function* updateCollectionItemSaga ({ payload: { id, updater } = {} }) {
  try {
    const collection = yield call([DB, "updateItem"], id, updater);
    yield put(getCollectionSuccess(collection));
  } catch (e) {
    yield put(getCollectionFailure(e));
  }
}

export function* exportCollectionSaga () {
  const collection = yield select(collectionSelector);
  const file = new Blob([JSON.stringify(collection)], {
    type: "application/json"
  });
  FileSaver.saveAs(
    file,
    `${moment.now()}-collection.${COLLECTION_FILE_EXTENSION}`
  );
}

export default function*() {
  yield takeEvery(GET_COLLECTION, getCollectionSaga);
  yield takeLatest(UPDATE_COLLECTION_ITEM, updateCollectionItemSaga);
  yield takeLatest(EXPORT_COLLECTION, exportCollectionSaga);
}
