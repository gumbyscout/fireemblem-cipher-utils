import { findIndex, slice } from "lodash/fp";

export const upsertInArray = matcher => item => array => {
  const index = findIndex(matcher, array);
  if (index === -1) {
    return [...array, item];
  }
  const firstSlice = slice(0, index, array);
  const secondSlice = slice(index + 1, array.length);
  return [...firstSlice, item, ...secondSlice];
};
