import { handleActions } from "redux-actions";
import { createSelector } from "reselect";

import CardIconUtils from "../services/cardIconUtils";

import { currentDeckSelector } from "../ManageDecks";

import {
  keyBy,
  flow,
  get,
  keys,
  compact,
  map,
  orderBy,
  partition,
  assign,
  sortBy,
  sum,
  mapValues,
  groupBy,
  filter
} from "lodash/fp";
const mapUncapped = map.convert({ cap: false });

import { GET_CARDS, TOGGLE_IS_MAXIMIZED, TOGGLE_DRAWER } from "../actions";

export const initialState = {
  cards: [],
  isMaximized: false,
  drawerIsOpen: true
};

const cards = handleActions(
  {
    [GET_CARDS]: (state, { payload }) => ({
      ...state,
      cards: payload
    }),
    [TOGGLE_IS_MAXIMIZED]: (state, { payload }) => ({
      ...state,
      isMaximized: payload
    }),
    [TOGGLE_DRAWER]: state => ({
      ...state,
      drawerIsOpen: !state.drawerIsOpen
    })
  },
  initialState
);

export default cards;

export const cardsSelector = get("App.cards");

export const cardsMapSelector = createSelector(
  cardsSelector,
  keyBy("_id")
);

export const cardsInCurrentDeckSelector = createSelector(
  currentDeckSelector,
  cardsMapSelector,
  (currentDeck, cardsMap) =>
    flow(
      get("cards"),
      mapUncapped((count, id) => {
        const card = get(id, cardsMap);
        return (
          card && {
            ...card,
            count,
            mainCharacter: card.name === currentDeck.mainCharacter
          }
        );
      }),
      compact,
      filter(({ count }) => count > 0),
      sortBy(["name", "cost"])
    )(currentDeck)
);

export const countOfCardsInCurrentDeckSelector = createSelector(
  cardsInCurrentDeckSelector,
  flow(
    map("count"),
    sum
  )
);

export const currentDeckColorCountsSelector = createSelector(
  cardsInCurrentDeckSelector,
  flow(
    groupBy("set"),
    mapValues(
      flow(
        map("count"),
        sum
      )
    )
  )
);

export const printSelector = createSelector(
  currentDeckSelector,
  cardsMapSelector,
  (deck, cardsMap) =>
    flow(
      get("cards"),
      compact(count => count),
      keys,
      map(cardId => {
        let card = cardsMap[cardId];
        if (card) {
          return assign(card, {
            thumbnail: CardIconUtils.getThumbnail(card),
            parsedSkills: card.skills.map(CardIconUtils.parseSkill),
            setIcon: CardIconUtils.getSetIcon(card.set),
            promotion: card.promotion === "N/A" ? null : card.promotion
          });
        }
        return null;
      }),
      compact,
      orderBy(["name", "cost"], ["asc", "asc"]),
      partition({ name: deck.mainCharacter })
    )(deck)
);
