import {handleActions} from 'redux-actions';

import {
  SEARCH_SET_FILTER,
  SEARCH_SET_QUERY,
  SEARCH_SET_ROW,
  SEARCH_SET_SORT
} from '../actions';

export const initialState = {
  filter: [],
  query: '',
  row: 0,
  sort: {
    name: 'Series (Ascending)',
    properties: ['series'],
    orders: ['asc']
  }
};

/* eslint-disable no-magic-numbers */
export default handleActions({
  [SEARCH_SET_QUERY]: (state, {payload}) => ({
    ...state,
    query: payload,
    row: 0
  }),
  [SEARCH_SET_FILTER]: (state, {payload}) => ({
    ...state,
    filter: payload,
    row: 0
  }),
  [SEARCH_SET_ROW]: (state, {payload}) => ({
    ...state,
    row: payload
  }),
  [SEARCH_SET_SORT]: (state, {payload}) => ({
    ...state,
    sort: payload,
    row: 0
  })
}, initialState);
/* eslint-enable */
