import {handleActions} from 'redux-actions';
import {SET_CARD_ISSUE} from  '../actions';

export const initialState = {
  issue: ''
};

export default handleActions({
  [SET_CARD_ISSUE]: (state, {payload}) => ({
    issue: payload
  })
}, initialState);
