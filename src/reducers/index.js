import { combineReducers } from "redux";

import App from "./App";
import CardSearch from "./CardSearch";
import ReportCard from "./ReportCard";
import { reducer as CardDetail } from "../CardDetail";
import { reducer as Collection } from "../Collection";
import { reducer as ManageDecks } from "../ManageDecks";

const rootReducer = combineReducers({
  App,
  CardSearch,
  ReportCard,
  CardDetail,
  Collection,
  ManageDecks
});

export default rootReducer;
