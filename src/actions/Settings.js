import {createAction} from 'redux-actions';

import {
  DATABASE_IS_LOADING,
  DATABASE_IS_DUMPING,
  DATABASE_IS_UPDATING
} from '../actions';

export const setIsLoading = createAction(DATABASE_IS_LOADING);
export const setIsDumping = createAction(DATABASE_IS_DUMPING);
export const setIsUpdating = createAction(DATABASE_IS_UPDATING);
