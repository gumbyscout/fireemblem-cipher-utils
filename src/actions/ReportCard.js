import {createAction} from 'redux-actions';
import {SET_CARD_ISSUE} from '../actions';

export const setIssue = createAction(SET_CARD_ISSUE);
