import { createAction } from "redux-actions";

import { GET_CARDS, TOGGLE_IS_MAXIMIZED, TOGGLE_DRAWER } from "../actions";

import CardDatabase from "../services/cardDatabase";

export const toggleIsMaximized = createAction(TOGGLE_IS_MAXIMIZED);

export const toggleDrawer = createAction(TOGGLE_DRAWER);

export const getCards = createAction(GET_CARDS, (doUpdate, noImages) =>
  CardDatabase.getCards(doUpdate, noImages)
);
