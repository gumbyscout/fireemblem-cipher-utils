import {createAction} from 'redux-actions';

import {
  SEARCH_SET_FILTER,
  SEARCH_SET_QUERY,
  SEARCH_SET_ROW,
  SEARCH_SET_SORT
} from '../actions';

export const setFilter = createAction(SEARCH_SET_FILTER);
export const setQuery = createAction(SEARCH_SET_QUERY);
export const setRow = createAction(SEARCH_SET_ROW);
export const setSort = createAction(SEARCH_SET_SORT);
