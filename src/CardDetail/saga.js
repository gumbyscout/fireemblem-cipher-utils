import { takeLatest, put, call } from "redux-saga/effects";

import { GET_CARD, getCardSuccess, getCardFailure } from "./reducer";
import CardDatabase from "../services/cardDatabase";

export function* getCardSaga ({ payload: id }) {
  try {
    const card = yield call([CardDatabase, "getCard"], id);
    yield put(getCardSuccess(card));
  } catch (e) {
    yield put(getCardFailure(e));
  }
}

export default function*() {
  yield takeLatest(GET_CARD, getCardSaga);
}
