import { handleActions, createAction } from "redux-actions";
import { createSelector } from "reselect";
import { get } from "lodash/fp";

import { currentDeckSelector } from "../ManageDecks";
import { collectionMapSelector } from "../Collection";

/*
 █████   ██████ ████████ ██  ██████  ███    ██ ███████
██   ██ ██         ██    ██ ██    ██ ████   ██ ██
███████ ██         ██    ██ ██    ██ ██ ██  ██ ███████
██   ██ ██         ██    ██ ██    ██ ██  ██ ██      ██
██   ██  ██████    ██    ██  ██████  ██   ████ ███████
*/

export const GET_CARD = "GET_CARD";
export const GET_CARD_SUCCESS = "GET_CARD_SUCCESS";
export const GET_CARD_FAILURE = "GET_CARD_FAILURE";

export const getCard = createAction(GET_CARD);
export const getCardSuccess = createAction(GET_CARD_SUCCESS);
export const getCardFailure = createAction(GET_CARD_FAILURE);

/*
██████  ███████ ██████  ██    ██  ██████ ███████ ██████
██   ██ ██      ██   ██ ██    ██ ██      ██      ██   ██
██████  █████   ██   ██ ██    ██ ██      █████   ██████
██   ██ ██      ██   ██ ██    ██ ██      ██      ██   ██
██   ██ ███████ ██████   ██████   ██████ ███████ ██   ██
*/

const initialState = {
  loading: true,
  card: {},
  error: null
};

export default handleActions(
  {
    [GET_CARD]: state => ({ ...state, loading: true }),
    [GET_CARD_SUCCESS]: (state, { payload: card }) => ({
      ...state,
      loading: false,
      card,
      error: null
    }),
    [GET_CARD_FAILURE]: (state, { payload: error }) => ({
      ...state,
      error,
      loading: false
    })
  },
  initialState
);

/*
███████ ███████ ██      ███████  ██████ ████████  ██████  ██████  ███████
██      ██      ██      ██      ██         ██    ██    ██ ██   ██ ██
███████ █████   ██      █████   ██         ██    ██    ██ ██████  ███████
     ██ ██      ██      ██      ██         ██    ██    ██ ██   ██      ██
███████ ███████ ███████ ███████  ██████    ██     ██████  ██   ██ ███████
*/

export const cardSelector = get("CardDetail.card");

export const isLoadingSelector = get("CardDetail.loading");

export const numInDeckSelector = createSelector(
  cardSelector,
  currentDeckSelector,
  ({ _id }, currentDeck) => get(["cards", _id], currentDeck)
);

export const numInCollectionSelector = createSelector(
  cardSelector,
  collectionMapSelector,
  ({ _id }, collectionMap) => get([_id, "count"], collectionMap)
);
