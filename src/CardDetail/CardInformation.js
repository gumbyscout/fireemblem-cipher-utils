import React from "react";
import PropTypes from "prop-types";
import CSSModules from "react-css-modules";

import get from "lodash/fp/get";
import isEmpty from "lodash/fp/isEmpty";
import map from "lodash/fp/map";
const mapUncapped = map.convert({ cap: false });

import Paper from "material-ui/Paper";

import CardIconUtils from "../services/cardIconUtils";

import styles from "./CardInformation.pcss";

@CSSModules(styles)
class CardInformation extends React.Component {
  static propTypes = {
    card: PropTypes.object.isRequired
  };

  render () {
    this.card = this.props.card;
    return this.card ? (
      <Paper>
        <div className={styles["card-header"]}>
          <div className={styles["card-title"]}>
            <img
              data-tip={this.card.set}
              className={styles["set-icon"]}
              src={CardIconUtils.getSetIcon(this.card.set)}
            />
            {`${this.card.name}, ${this.card.title}`}
          </div>
          <div>
            {map(aff => {
              const affinity = CardIconUtils.icons[aff];
              return affinity ? (
                <img
                  key={aff}
                  src={CardIconUtils.getAffinityIcon(aff)}
                  data-tip={get("name")(affinity)}
                />
              ) : null;
            })(this.card.affinities)}
          </div>
        </div>
        <div className={styles["card-content"]}>
          <div className={styles["image-wrapper"]}>
            <img src={CardIconUtils.getThumbnail(this.card)} />
          </div>
          <div className={styles["meta-wrapper"]}>
            <table style={{ width: "100%" }}>
              <tbody>
                <tr styleName="meta-item">
                  <td>Series</td>
                  <td>{this.card.series}</td>
                  <td>Cost</td>
                  <td>{`${this.card.cost} ${
                    this.card.promotion === "N/A" ||
                    isEmpty(this.card.promotion)
                      ? ""
                      : "(" + this.card.promotion + ")"
                  }`}</td>
                </tr>
                <tr styleName="meta-item">
                  <td>Attack</td>
                  <td>{this.card.attack}</td>
                  <td>Support</td>
                  <td>{this.card.support}</td>
                </tr>
                <tr styleName="meta-item">
                  <td>Range</td>
                  <td>{this.card.range}</td>
                  <td>Class</td>
                  <td>{this.card.class}</td>
                </tr>
              </tbody>
            </table>
            {mapUncapped((skill, index) => (
              <div key={index} styleName="skill">
                <span
                  dangerouslySetInnerHTML={{
                    __html: CardIconUtils.parseSkill(skill)
                  }}
                />
              </div>
            ))(this.card.skills)}
          </div>
        </div>
      </Paper>
    ) : null;
  }
}

export default CardInformation;
