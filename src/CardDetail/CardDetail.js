import React from "react";
import PropTypes from "prop-types";
import CSSModules from "react-css-modules";
import { connect } from "react-redux";

import { get, isEmpty } from "lodash/fp";
import ReactTooltip from "react-tooltip";
import { Toolbar, ToolbarGroup, ToolbarTitle } from "material-ui/Toolbar";
import FlatButton from "material-ui/FlatButton";
import ArrowBack from "material-ui/svg-icons/navigation/arrow-back";

import {
  cardSelector,
  numInCollectionSelector,
  numInDeckSelector,
  getCard,
  isLoadingSelector
} from "./reducer";
import { currentDeckSelector, updateCurrentDeck } from "../ManageDecks";
import { updateCollectionItem } from "../Collection";
import CardInformation from "./CardInformation";
import styles from "./styles.pcss";

@CSSModules(styles)
class CardDetail extends React.Component {
  componentDidMount () {
    this.updateCurrentCard();
  }

  componentWillUpdate (nextProps) {
    // TODO: useEffect when I can use hooks.
    this.updateCurrentCard(nextProps);
  }

  updateCurrentCard = nextProps => {
    const { card = {}, getCard } = this.props;
    const _id = get("routeParams.id", nextProps || this.props);
    if (isEmpty(card) || card._id !== _id) {
      getCard(_id);
    }
  };

  incrementCardInDeck = () => {
    const { numInDeck = 0 } = this.props;
    this.changeDeckCount(numInDeck + 1);
  };

  decrementCardInDeck = () => {
    const { numInDeck = 0 } = this.props;
    this.changeDeckCount(numInDeck - 1);
  };

  changeDeckCount = e => {
    const number = parseInt(get("target.value", e)) || parseInt(e) || 0;
    const { changeCardCount, card } = this.props;
    changeCardCount(card._id, number);
  };

  changeCollectionCount = e => {
    const count = parseInt(get("target.value", e)) || parseInt(e) || 0;
    const { updateCollectionItem, card } = this.props;
    updateCollectionItem({ id: card._id, updater: c => ({ ...c, count }) });
  };

  render () {
    const {
      card,
      numInDeck,
      numInCollection,
      currentDeck,
      loading,
      setMainCharacter
    } = this.props;
    if (isEmpty(card) || loading) {
      return null;
    }

    return (
      <div className={styles["card-detail"]}>
        <Toolbar className={styles.toolbar}>
          <ToolbarGroup firstChild>
            <FlatButton
              label="Back"
              icon={<ArrowBack />}
              onClick={this.context.router.goBack}
            />
          </ToolbarGroup>
          {!isEmpty(currentDeck) ? (
            <ToolbarGroup>
              <FlatButton onClick={() => setMainCharacter(card)}>
                Set Main Character
              </FlatButton>
              <ToolbarTitle text="Deck:" />
              <FlatButton onClick={this.incrementCardInDeck}>+1</FlatButton>
              <input
                value={numInDeck || 0}
                onChange={this.changeDeckCount}
                type="number"
              />
              <FlatButton onClick={this.decrementCardInDeck}>-1</FlatButton>
            </ToolbarGroup>
          ) : null}
        </Toolbar>
        <div styleName="card-wrapper">
          <CardInformation card={card} />
        </div>
        <ReactTooltip />
      </div>
    );
  }
}

CardDetail.contextTypes = {
  router: PropTypes.object.isRequired
};

CardDetail.propTypes = {
  changeCardCount: PropTypes.func.isRequired,
  routeParams: PropTypes.object.isRequired,
  updateCollectionItem: PropTypes.func.isRequired,
  card: PropTypes.object.isRequired,
  numInDeck: PropTypes.number,
  numInCollection: PropTypes.number,
  currentDeck: PropTypes.object,
  getCard: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  setMainCharacter: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  card: cardSelector(state),
  numInDeck: numInDeckSelector(state),
  numInCollection: numInCollectionSelector(state),
  currentDeck: currentDeckSelector(state),
  loading: isLoadingSelector(state)
});

const mapDispatchToProps = dispatch => ({
  changeCardCount: (id, count) =>
    dispatch(
      updateCurrentDeck(deck => ({
        ...deck,
        cards: {
          ...deck.cards,
          [id]: count < 0 ? 0 : count
        }
      }))
    ),
  setMainCharacter: card =>
    dispatch(
      updateCurrentDeck(deck => ({
        ...deck,
        mainCharacter: card.name
      }))
    ),
  updateCurrentDeck: deck => dispatch(updateCurrentDeck(deck)),
  updateCollectionItem: item => dispatch(updateCollectionItem(item)),
  getCard: id => dispatch(getCard(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CardDetail);
