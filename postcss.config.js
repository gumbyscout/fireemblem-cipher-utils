module.exports = {
  modules: true,
  plugins: [
    require("postcss-nested"),
    require("lost"),
    require("postcss-short"),
    require("postcss-simple-vars")({
      variables: () => require("./src/theme.js")
    })
  ]
};
